# CAD Models

Files for creating the In-&-Out-Box.

## Folder information

### CAD Models
Autodesk Inventor CAD files / .ipt files = individual component / .iam files = assemblies

### ai
Adobe Illustrator files - converted CAD models needed for lasercutting.

### dxf
Derived Sketches